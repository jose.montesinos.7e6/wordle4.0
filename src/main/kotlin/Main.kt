
import java.nio.file.Path
import java.time.LocalDate
import kotlin.io.path.*
/**
 *Juego Wordle.
 * Tienes que adivinar una palabra de 5 letras generada aleatoriamente por el programa.
 * Tienes 6 intentos para adivinarla.
 * Después de estos 6 intentos, tu decides si quieres volver a jugar.
 * Si tu palabra contiene la misma letra en la misma
 * posición que la palabra generada, la letra saldrá por pantalla de color verde.
 * Si tu palabra contiene la misma letra pero en diferente posición a la palabra generada,
 * la letra saldrá por pantalla de color amarillo.
 * Sólo saldrán de color amarillo aquellas letras que coincidan en número.
 * Ejemplo: la palabra generada es "patito" y escribo "patata", sólo me saldrá de color verde
 * la primera "a", el resto de color gris porque "patito" sólo contiene una "a" y patata
 * contiene 3 "a", 2 de ellas sobran.
 * @ author: Jose Montesinos
 * @ version: 4.0
 */

//********************Variables de los colores*****************!!!
var green= "\u001b[48;2;0;204;0m"   //verde
var yellow= "\u001b[48;2;240;234;39m"   //amarillo
var grey= "\u001b[48;2;160;160;160m"   //gris
var black="\u001b[38;2;0;0;0m"   //fondo negro
var reset= "\u001b[m"

//**********************Variables de las listas*****************!!!


/*
*Lista de 5 elementos String dónde almacenamos los colores (en String) con los que se imprimiran por pantalla
* cada una de las letras que hemos introducido
 */
var colors: MutableList<String> = mutableListOf()

/*
*Lista de sublistas de 2 elementos, donde añadimos en cada sublista, en números enteros, las posiciones dónde coinciden las letras iguales de la palabra
* introducida por el usuario respecto y de la palabra creada por la máquina. A la hora de hacer el bucle, si 2 letras coinciden
* pero detecta que esas posiciones ya están en una sublista, pasará de largo hasta encontrar una
* letra que coincida en una posición que no esté en esta lista.
 */
var positions:MutableList<MutableList<Int>> = mutableListOf(mutableListOf())

/*Funcion que te escoge aleatoriamente una palabra en castellano del archivo "ListWordsSpanish.txt"
*@return: String, palabra aleatoria
 */
fun machineWordCastella():String{
    val pathListSpanish = Path("./ListWords/ListWordsSpanish.txt")
    val lines:List<String> = pathListSpanish.readLines()
    val aleatoriWord = lines.random().lowercase()
    //println(aleatoriWord)
    return aleatoriWord
}

/*Funcion que te escoge aleatoriamente una palabra en catalan del archivo "ListWordsCatalan.txt"
*@return: String, palabra aleatoria
 */
fun machineWordCatalan():String{
    val pathListCatalan = Path("./ListWords/ListWordsCatalan.txt")
    val lines: List<String> = pathListCatalan.readLines()
    val aleatoriWord = lines.random().lowercase()
    //println(aleatoriWord)
    return aleatoriWord
}

/*Función que escoge la palabra en castellano o en catalán según la elección del usuario
*@parem: Int, número con el que se escoge el idioma de la palabra generada
*@return: String, la palabra aleatoria de la máquina en un idioma específico
 */
fun seleccion(numero:String):String{
    if(numero=="1"){
        println("Se ha generado una palabra en Castellano")
        return machineWordCastella()
    }else{
        println("Se ha generado una palabra en catalán")
        return machineWordCatalan()
    }

}

/*Función que tiene por entrada la palabra aleatoria generada por la máquina y la palabra introducida por el usuario.
*La salida será una lista con 2 sublistas, una de ellas contendrá las letras de la palabra introducida por el usuario
*y cada una de estas letras ocuparán una posición de esta sublista. La segunda sublista tendrá un tamaño de 5 posiciones
*(al igual que el número de letras de la plabara) y en cada posicion se guardará el color que le pertenece a cada letra en
*formato String.
* La función hará un análisis comparando cada letra y las posiciones donde se encuentran para responder de la manera correcta.
* @parem: String, choiceWord es la palabra introducida por el usuario
* @parem: String, aleatoriWord es la palabra aleatoria generada por la máquina
* @return: MutableList<MutableList<String>>, lista mutable con 2 sublistas mutables dónde, en la primera, irán las letras de la palabra
* introducida por el usuario y en la segunda, los colores que le pertenecen a cada letra.
 */
fun analysis(choiceWord:String, aleatoriWord: String):MutableList<MutableList<String>>{

    val letras:MutableList<String> = mutableListOf()   //Lista mutable donde añadiremos las letras de la palabra introducida por el usuario

    positions.remove(mutableListOf())    /*Lista con sublistas de 1 o 2 posiciones, donde en cada sublista, añadiremos la poscion de la letra
                                            introducida por el usuario y la posición de la letra de la palabra generada aleatoria en caso
                                            de que estás coincidan, en caso contrario solo añadimos la posicion de la letra que pintaremos de
                                            color gris*/

    //Variables que se activan o no en función del color que tendremos que pintar las letras
    var green: Boolean = false
    var yellow: Boolean = false
    var grey: Boolean = false

    for (i in 0..choiceWord.length - 1) {      //Bucle superior que recorre cada letra de la palabra introducida por el usuario
        for (j in 0..aleatoriWord.length - 1) {      //Bucle inferior que recorre cada letra de la palabra aleatoria
            if (i == j && choiceWord[i] == aleatoriWord[j]) {     //Condicional que compara las 2 letras y las posiciones de ambas
                changeColorGrey(i,j)    //Se llama a la función que cambie de color a la letra
                positions.add(mutableListOf(i, j))      //Se añaden en Int las posiciones de las letras de la palabra Introducida y de la palabra aleatoria a la lista "posiciones"
                green = proveGreen() //Se llama a la función que cambia el estado de la variable "green"
                break
            } else if (choiceWord[i] == aleatoriWord[j]) {    //Condicional que compara las letras
                yellow = proveYellow(i, j)    //Se llama a la función que pintará las letras de color amarillo
            } else {
                grey = true    //Si ningun caso anterior se cumple, se cambiará el estado de la variable "grey"
            }
        }

        //Una vez acabado el bucle, se añaden a la sublista de colores el color en función de como se han activado las variables
        //green, yellow y grey

        if (green) {
            colors.add("green")
        } else if (yellow) {
            colors.add("yellow")
        } else {
            colors.add("grey")
            positions.add(mutableListOf(i))
        }

        //Una vez se añaden, se vuelven a desactivar estas variables y empezamos el ciclo de nuevo
        green = false
        yellow = false
        grey = false

        //Antes de acabar el bucle superior, añadimos la letra a la sublista de letras
        letras.add(choiceWord[i].toString())

    }

    //println(colors)
    //println(positions)
    //println(colors)
    return mutableListOf(colors, letras)   //Devolvemos la lista con las 2 sublistas
}

/*
* Función que devolverá un booleano true para pintar la letra de color verde
 */
fun proveGreen():Boolean{
    return true
}

/*Función que, una vez se haya detectado una letra que tenga que pintarse de color verde,
* si existe con anterioridad una letra pintada de color amarillo, sepa si esta tenga que cambiarse
* a ser pintada de color gris.
* @parem: Int, posicion de la letra de la palabra introducida.
* @parem: Int, posición de la letra de la palabra aleatoria.
 */
fun changeColorGrey(i:Int,j:Int){
    var change = false   //variable Boolean que se activa si la letra amarilla tiene que cambiar a gris
        var newColor = 0   //Variable donde se guardara la posicion donde cambiaremos el color en la sublista de colores
        for (l in 0..positions.size - 1) {    //Bucle que recorre la lista donde guardamos las posiciones donde se han encontrado 2 letras iguales
            if(positions[l].size>1){    //Condicional que nos dice si la sublista en posiciones es mayor a uno (no es una posicion de una letra gris)
                for(k in 0..positions[l].size-1) {     //Bucle de la sublista de 2 posiciones
                    if (positions[l][k] == j) {
                        change = true
                        newColor = l
                    }
                }
            }
        }
        if(change){colors[newColor] = "grey"}   //Si se cumple, se cambia el color amarillo a gris en la lista colores
}

/*
* Función que comprobara si una letra tiene que ser pintada de color amarillo
* @parem: Int, posicion de la letra de la palabra introducida por el usuario
* @parem: Int, posición de la letra de la palabra generada aleatoriamente por la máquina
* @return: Boolean, valor que nos dirá si tenemos que pintar la letra de color amarillo
*/
fun proveYellow(i:Int,j:Int):Boolean {
    var add = true    //Variable que nos dice si tenemos que añadir una sublista de las posiciones i,j en la lista de posiciones
    var color = true    //Variable que se devolverá a la función "analysis" y que nos dirá si pintamos la letra en amarillo o no

        for (k in 0..positions.size - 1) {     //Bucle que recorre la lista donde guardamos las posiciones donde se han encontrado 2 letras iguales
            if(positions[k].size>1) {     //Condicional que nos dice si la sublista en posiciones es mayor a uno (no es una posicion de una letra gris)
                for (l in 0..positions[k].size - 1) {      //Bucle de la sublista de 2 posiciones
                    if (positions[k][l] == j && l>0) {
                        add = false

                        color = false     //Si encontramos la posicion j en la lista posiciones, no se pintará de color amarillo
                    }
                }
            }
        }

    if(add){positions.add(mutableListOf<Int>(i,j))}    //Condicional que nos dice que si add es cierto, añadiremos estas 2 posiciones en una sublista, y esta dentro de la lista posiciones
    return color  //Por último devolvemos si se pintara de color amarillo o no
}

/*Función que tendrá por entrada una lista con 2 sublistas, colores y printará por pantalla las letras con el color pedido
* @parem: MutableList<MutableList<String>>, lista con 2 sublistas en las que están los colores que hay que pintar cada letra y las letras de la palabra introducida por el usuario
 */
fun printar(salida:MutableList<MutableList<String>>){
    for(i in 0..salida[0].size-1){   //Bucle que itera en cada elemento de la sublista colores
        if(salida[0][i]=="green"){    //Si se encuentra verde en la lista, lo pinta de ese color
            salida[0][i]= "\u001b[48;2;0;204;0m"
        }else if(salida[0][i]=="yellow"){
            salida[0][i]="\u001b[48;2;240;234;39m" //Si se encuentra amarillo en la lista, lo pinta de ese color
        }else{
            salida[0][i]="\u001b[48;2;160;160;160m"  //Si se encuentra gris en la lista, lo pinta de ese color
        }
    }

    println(salida[0][0]+black+salida[1][0]+reset+salida[0][1]+black+salida[1][1]+reset+salida[0][2]+black+salida[1][2]+reset+salida[0][3]+black+salida[1][3]+reset+salida[0][4]+black+salida[1][4]+reset)
}
fun main() {
    var victorias = 0      //Variable que cuenta las veces que adivinas el número
    var derrotas = 0       //Variable que cuenta las veces que te quedas sin intentos en una partida
    var intentosUtilizados = 0    //Variable que cuenta los intentos totales que has hecho en todas las partidas



    do {     //Bucle que te permite jugar mientras escojas "s"
        println("Bienvenido a Worlde. Primero de todo selecciona en que idioma quieres que esté la plabara a adivinar\nPresiona 1:Castellano o 2:Catalan")
        var idioma = readln()   //var donde se introduce el idioma escogido
        while(idioma!="1" && idioma!="2"){
            println("Por favor, introduzca 1 si quiere generar la palabra en Castellano o 2 si la quiere generar en catalán.")
            idioma = readln()
        }

        var intents = 6    //Variable que cuenta los intentos que tienes en una partida

        var aleatoriWord = seleccion(idioma)   //Se llama a la función que escoge la palabra, te la devuelve y se iguala a la variable aleatoriWord

        do {    //bucle que te permite jugar mientras los intentos sean mayores a 6 y la palabra introducida sea diferente a la palabra aleatoria
            var choiceWord = readln().lowercase()
            while (choiceWord != String() && choiceWord.length != 5) {
                println("Error, introduzca una palabra de 5 letras")
                choiceWord = readln().lowercase()
            }

            val salida = analysis(choiceWord, aleatoriWord)    //La función análisis se introduce en una variable llamada salida

            printar(salida)    //La función printar escribirá por pantalla la variable salida

            positions.clear()    //Se limpia la lista de posiciones después de printar la palabra
            colors.clear()      //Se limpia la lista colores después de printar la palabra
            intents--
            intentosUtilizados++
            println("")

        } while (intents > 0 && aleatoriWord != choiceWord)

        if (intents == 0) {
            println("Te has quedado sin intentos, la palabra era $aleatoriWord")
            derrotas++
        } else {
            println("Enhorabuena! Has hacertado que la palabra era $aleatoriWord")
            victorias++
        }

        println("Quieres volver a jugar? Presiona tecla 's' si es así, en caso contrario, pulsa 'n'.\n" +
                "Si quiere ver sus estadisticas actuales pulse 1")
        var repetir = readln()

        while (repetir != "s" && repetir != "n" && repetir!="1") {
            println("Elija una de las 2 opciones.")
            println("Quieres volver a jugar? Presiona tecla 's' si es así, en caso contrario, pulsa 'n'")
            repetir = readln()
        }
        if (repetir == "n") {
            println("Vuelva a jugar cuándo quiera!")
            repetir = verEstadisticas("n", victorias, derrotas, intentosUtilizados)
        }
        else if(repetir=="1"){
            repetir = verEstadisticas("1",victorias, derrotas, intentosUtilizados)
        }
    } while (repetir == "s" || repetir == "1")
    val date = LocalDate.now()
    var existPartida = true;
    var i = 0
    while(existPartida){
        i++
        val datosPartida = Path("./EstadisticasPartidas/Partida${i} - ${date}")
        existPartida = datosPartida.exists()
    }
    val datosPartida = Path("./EstadisticasPartidas/Partida${i} - ${date}")
    datosPartida.createFile()
    datosPartida.appendText("Derrotas: $derrotas \n" +
            "Victorias: $victorias \n" +
            "Intentos totales: $intentosUtilizados")

}

fun verEstadisticas(parametro:String, victorias: Int, derrotas:Int, intentosUtilizados:Int):String{
    if(parametro == "1"){
        println("""
        Sus estadísticas actuales son son:
        - Derrotas: $derrotas
        - Victorias: $victorias
        - Intentos totales: $intentosUtilizados
    """.trimIndent())
        println("Pulse un boton y vuelva a jugar una partida")
        var volver = readln()

    }else{
        println("""
        Sus estadísticas finales son:
        - Derrotas: $derrotas
        - Victorias: $victorias
        - Intentos totales: $intentosUtilizados
    """.trimIndent())
    }
    return parametro
}





