#   Wordle versión 1.0
## Descripción
El juego empieza con todas las variables que harán que las letras cambien de color al acertarlas en su posición (verde), al indicar que la letra está en la palabra aleatoria (amarillo) y cuando la letra no esta en la palabra aleatoria (gris).
Después encontramos la función math.random() y la asociamos al array de palabras que tenemos para que salga una palabra aleatoria cada vez que juguemos. Creamos una variable con 6 intentos y a continuación introducimos una entrada para la palabra del usuario.
El juego tiene 3 bucles while y un if, el primer bucle nos obliga a introducir una palabra de 5 letras, el segundo while nos obliga a introducir una palabra que exista en el array y el último while es el principal que hace que el juego sea posible.
Dentro del tercer while empezamos decrementando los intentos, creamos una variable j y la iniciallizamos en 0. Continuamos con un bucle for y por cada letra "i" en la palabra introducida, la comparamos con la letra de la palabra aleatoria en la posicion "j" (0 al inicio), si son iguales se pondran de color verde, si no irá a la segunda opción, la cuál nos dice que si la palabra aleatoria contiene la letra "i" de la palabra introducida, la pinte de color amarillo, en caso de que no se cumpliera ninguna de las 2 condiciones anteriores, la pinta de color gris. Seguidamente suma 1 a la variable "j" y continua con el for en i = 1. Una vez acaba nos dejará introducir otra palabra.
Si acertamos saldremos del bucle while pero si nos quedamos sin intentos, antes de salir del bucle while, el juego pasará por una condición (alternativa a la de continuar jugando) que nos dirá que nos hemos quedado sin intentos.
Una vez salimos del bucle while principal, si y sólo si hemos acertado la palabra, nos dirá que el resultado es correcto.
## Objetivos
Aprender a utilizar la lógica y dar ordenes precisas al programa en caso de utilizar listas de palabras y analizar cada una de estas palabras de manera coherente para que el juego funcione correctamente.
## Variables y valores
val green: se usa para poner de verde las letras en la posición correcta
val yell: se usa para poner de amarillo las letras que están en la palabra aleatoria pero en una posicion incorrecta.
val grey: se usa para poner de gris las letras que no están en la palabra aleatoria.
val black: se usa para poner las letras negras cuando el fondo es verde o amarillo.
val reset: se usa para no pintar toda la fila con el fondo del color verde, amarillo o gris.
val entrada: se le asocia la variable "palabra"
var intentos: cuenta los intentos que nos quedan en el juego (inicialmente 6).
var palabra: variable donde guardamos la palabra introducida.
val palabras: valor del array de palabras que pueden salir como aleatorias.
val aleatorio: número aleatorio que va desde 0 hasta palabras.length-1.
val palabraAle: palabra del array que esta asociada al número aleatorio y que será la palabra a adivinar.
var j: contador que que asocia la posición de la letra en la palabraAle (palabra aleatoria).
## Cosas a mejorar
Podríamos darle la opción al jugador si quiere seguir jugando o si quiere abandonar el juego aunque le queden uno o varios intentos. También podriamos mejorar la apariéncia de las palabras en la consola. Se podría utilizar un sistema de juego a base de puntos, dónde un jugador pierde puntos por cada intento fallido y gana puntos si adivina la palabra, también podríamos poner un marcador de puntos para 2 o más jugadores.

----------------------------------------------------------------------------------------------------------------------------------------

### Wordle Version 2.0
En esta segunda versión de wordle he puesto una cierta cantidad de código adicional para que las letras introducidas que sean iguales a las letras de la palabra generada aleatoriamente, sepan si esa letra se repite o no y tenga que ponerse de color amarillo o gris.
He separado cierta parte del código en funciones y las he testeado, están dentro del archivo MinKtTest.kt.

----------------------------------------------------------------------------------------------------------------------------------------

### Wordle Versión 4.0
En esta cuarta y última versión de Wordle, he introducido una cantidad de código mucho mayor en las funciones y he mejorado el sistema que detecta si las palabras se repiten. En la anterior versión, detectaba si una letra ya había sido encontrada mediante una lista, funcionaba para letras del color amarillo pero en caso de salir una verde, y la anterior letra igual a esta estaba en color amarillo, esta ya no podía cambiar al color gris porque ya había sido printada. Ahora en lugar de una lista, hay una matriz donde se guarda tanto la posicion de la letra de la palabra aleatoria como la posicion de la letra de la palabra introducida, además he creado una lista aparte donde se guardan los colores de cada letra antes de imprimirlas, de esta forma, si una letra verde detecta que otra letra igual habia sido seleccionada con el color amarillo anteriormente, se toma su posicion de fila y en esa misma posicion, se cambia el color a gris en la lista de colores y una vez hechos los cambios, se printea. 
También he añadido la opción de jugar adivinando palabras en catalán o en castellano, esta opción es elegida por el usuario. 
Además he puesto un recuento final de los intentos totales hechos por el usuario, las victorias que ha conseguido en diferentes partidas y las veces que ha perdido o que no ha podido resolver la palabra en los 6 intentos permitidos.
